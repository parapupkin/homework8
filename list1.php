<?php
require_once 'functions.php';
if (!isAuthorized()) {
      header("HTTP/1.1 403 Forbidden"); 
      exit();  
    }
//проверяем, есть ли папка с тестами, если нет - создаем
if (!file_exists(__DIR__ . "/tests")) {
    mkdir(__DIR__ . "/tests");
}
//если файл загружен во временную папку, то...
if(!empty($_FILES) && $_FILES['test']['error'] == 0) { 
// проверка на максимальный размер файла
$maxsize = 5000000; 
$size = filesize($_FILES['test']['tmp_name']);   
  if($size > $maxsize) {       
    echo "Для загрузки доступны файлы менее 5 Мб";  
    exit();     
  } 
//указываем папку для хранения тестов    
$destiation_dir = "tests" .'/'.$_FILES['test']['name'];
// запоминаем имя файла
$fileName = $_FILES['test']['name'];
//проверяем тип файла 
$info = new SplFileInfo($_FILES['test']['name']);
$infoType = $info->getExtension();
if ($infoType != "json") {
    echo "Для загрузки доступны только файлы json. Вернитесь на предыдущую страницу и загрузите корректный формат";
    exit();
}
// изменяем тип временного файла на json
$newType = stristr($_FILES['test']['tmp_name'], '.', true) . ".json";
$content_name = rename($_FILES['test']['tmp_name'], $newType); 
$content = file_get_contents($newType);
$result = json_decode($content, true);
//проверяем, чтобы там был массив
if (gettype($result) !== "array") {
    echo "Неверная структура json";
    unlink($newType);
    exit();
}
// Выполняем проверку структуры json
foreach ($result as $value) {
   if (array_key_exists('question', $value) == 0 || array_key_exists('answer1', $value) == 0 || array_key_exists('answer2', $value) == 0 || array_key_exists('answer3', $value) == 0 || array_key_exists('true', $value) == 0) {
    echo "Неверная структура json - не те ключи";
    unlink($newType);
    exit();
   } else { 
    foreach ($value as $answer) {
        if (gettype($answer) == "array") {
            echo "Неверная структура json - значения не могут быть массивами";
            unlink($newType);
            exit();
        }
     }    
  }    
}
//если все нормально - перемещаем файл и выдаем ссобщение о загрузке
rename($newType, "tests" .'/'. $fileName);
// move_uploaded_file($newType, ); 
$message = 'Новый тест загружен <br>';
}
else {
$message = 'Новый тест не загружен'; // Оповещаем пользователя о том, что файл не был загружен
} 

// сканируем папку хранения тестов и выдаем список
$dir ='tests';
$list=scandir($dir);
$z=count(scandir('tests'))-1;
$x=2;
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Weather in <?= $name ?></title>
  <link rel="stylesheet" type="text/css" href="style.css"> 
  <style type="text/css">
      html, .container {
                background: #5C5B91;
            }
  </style>
</head>
<body>    
  <div class="container">
    <p class="message"> <?php echo "$message";?></p>
    <p>Список загруженных тестов</p>
    <?php 
        //подсчитываем число файлов в папке с тестами, если они есть - составляем список
        $numberOfFiles = count(scandir(__DIR__ . "/tests")) - 2;        
        if ($numberOfFiles>0) {
             do {
              echo $x-1 . ". " . $list[$x] . "</br>";    
          }   while ($x++<$z);           
         } 
    ?>
    <form action="test.php" method="get" >
        <div class="border border1">Введите номер теста, который хотите пройти<br/>    
            <input type="text" name="number" value="0" /><br/>                
            <button type="submit">Выбрать этот тест</button><br/> 
        </div>
    </form>     
    <?php if (isset($_SESSION['user']['username']) && (isAdmin())): ?>
    <form action="test.php" method="get" >
        <div class="border border2">Введите номер теста, который хотите УДАЛИТЬ<br/>             
            <input type="text" name="delete" value="0" /><br/>                
            <button type="submit">Удалить этот тест</button> 
        </div> 
        <a href="admin.php">Загрузить еще один тест</a> 
        <?php endif; ?>        
    </form>    
  </div>
</body>
</html>