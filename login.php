<?php
require_once'functions.php';
    // если авторизован - переходим на list1
    if (isAuthorized()) {
        redirect('admin');
    }
    $errors = [];
    // выполняем проверку, зашел ли пользователь как гость
    if (!empty($_POST)) {
        if (isset($_POST['name'])) {            
            $_SESSION['name'] = $_POST['name'];
            redirect('admin');
        } 
        // выполняем проверку, соответствует ли логин и пароль
        foreach (getUsers() as $user) {
            if ($_POST['login'] == $user['login'] && $_POST['password'] == $user['password']) {
                $_SESSION['user'] = $user;
                redirect('admin');
            }
        }
        $errors[] = 'Неверный логин или пароль';
    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"> 
    <link rel="stylesheet" type="text/css" href="style.css">  
    <title>Авторизация</title>
</head>
<body>
<section id="login">
    <div class="container">        
        <h1>Авторизация</h1>
        <ul>
            <?php foreach ($errors as $error): ?>
            <li><?= $error ?></li>
            <?php endforeach; ?>
        </ul>
        <form method="POST">
            <div class="form-group">
                <label for="lg" class="sr-only">Логин</label> 
                <input type="text" placeholder="Логин" name="login" id="lg" class="form-control">
            </div>
            <div class="form-group">
                <label for="key" class="sr-only">Пароль</label>
                <input type="password"  placeholder="Пароль" name="password" id="key" class="form-control">
            </div>
            <input type="submit" id="btn-login" class="btn" value="Войти">
        </form> 
        <h2> Можно войти как гость, просто введите имя </h2>     
        <form method="POST">
            <div class="form-group">
                <label for="name" class="sr-only">Имя</label> 
                <input type="text" placeholder="Имя" name="name" id="name" class="form-control">
            </div>            
            <input type="submit" id="btn-login" class="btn" value="Войти как гость">
        </form>                    
    </div> <!-- /.container -->
</section>
</body>
</html>