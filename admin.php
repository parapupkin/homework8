<?php
    require_once 'functions.php';
    if (!isAuthorized()) {
      header("HTTP/1.1 403 Forbidden"); 
      exit();  
    }
    $url_admin = $_SERVER['HTTP_HOST'] . $_SERVER["REQUEST_URI"];
    $url_list = dirname($url_admin) . "/list1.php";     
    if(isset($_FILES["test"]) && !empty($_FILES["test"]) && $_FILES['test']['size'] !== 0) {
        header("Location: http://"  . $url_list, true, 307);        
    } 
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Форма для передачи данных методом POST</title>
        <style type="text/css">
            ul {
                list-style: none;
                font-size: 20px;                
            }
            ul li {
                margin-bottom: 20px;
            }
            html {
                background: #5C5B91;
            }
            form {
                margin-top: 20px;                
                text-align: center;
                font-size: 20px;
            }
            input {
                margin: 20px;
                font-size: 20px;
            }
            input:hover {
                border-color: #2A295F;
            }
            .container {
                text-align: center;
            }
            .container p {
                font-size: 22px;                
            }
            h1 {
                margin-top: 50px;
            }
        </style>
    </head>
    <body>        
        <div class="container">
            <h1>Добро пожаловать, <?php if (!empty($_SESSION['name'])) {
            echo $_SESSION['name'];
        } else { 
            echo $_SESSION['user']['username'];
        } ?>  </h1>
        
        <?php if (isset($_SESSION['user']['username']) && (isAdmin())): ?>
        <p>Заполните поле с именем и отправьте файл теста</p>               
        <form enctype="multipart/form-data" action="" method="post" >Введите имя   
            <input type="text" name="name" value="Василий Иванов" /><br/>
            <input type="file" name="test"><br/>            
            <input type="submit" name="bsubmit" value="Отправить" /> 
        </form>
        <?php endif; ?>
        <form action="list1.php" method="post">                       
            <input type="submit" name="bsubmit-pro" value="Смотреть доступные тесты" />
        </form>
        <ul>
            <li><a href="logout.php" >Выйти</a></li>            
        </ul>
        </div>  
    </body>
</html>


